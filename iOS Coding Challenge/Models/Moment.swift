//
//  Moment.swift
//  iOS Coding Challenge
//
//  Created by Phạm Phi Phúc on 5/31/17.
//  Copyright © 2017 Pham Phi Phuc. All rights reserved.
//

import UIKit

class Moment {
    var _id: Int!
    var message: String!
    var postAt: Date!
    var user: User!
    var imageURL: String!
    var likeCount: Int!
    var commentCount: Int!
}
