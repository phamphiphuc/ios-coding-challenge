//
//  User.swift
//  iOS Coding Challenge
//
//  Created by Phạm Phi Phúc on 5/31/17.
//  Copyright © 2017 Pham Phi Phuc. All rights reserved.
//

import UIKit

class User: NSObject {
    var _id: Int!
    var fullName: String!
    var mobileNo: String!
    var dob: Date!
    var imageURL: String!
}
