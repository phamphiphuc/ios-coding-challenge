//
//  JSONServices.swift
//  iOS Coding Challenge
//
//  Created by Phạm Phi Phúc on 6/1/17.
//  Copyright © 2017 Pham Phi Phuc. All rights reserved.
//

import Foundation

class JSONServices {
    
    static let shared = JSONServices()
    
    func parseMoments(data: Data!) -> [Moment]? {
        var moments: [Moment] = []
        
        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                let jsonData = json["data"] as? [[String: Any]] {
                
                for momentData in jsonData {
                    let moment = Moment()
                    moment._id = momentData["_id"] as! Int
                    moment.message = momentData["message"] as! String
                    moment.imageURL = momentData["imageURL"] as! String
                    moment.user = parseUser(data: momentData["user"] as! [String: Any])
                    moment.postAt = TimeServices.dateFromString(momentData["postedAt"] as! String)
                    moments.append(moment)
                }
            }
        } catch {
            return nil
        }
        
        return moments
    }
    
    fileprivate func parseUser(data: [String: Any]) -> User {
        let user = User()
        user._id = data["_id"] as! Int
        user.fullName = data["fullName"] as! String
        user.mobileNo = data["mobileNo"] as! String
        user.dob = TimeServices.dateFromString(data["dob"] as! String)
        user.imageURL = data["imageURL"] as! String
 
        return user
    }
    
    func parseLikeCount(data: Data!, moment: inout Moment) {
        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                moment.likeCount = json["data"] as! Int
            }
        } catch {
            print("Error parsing JSON: \(error)")
        }
    }
    
    func parseCommentCount(data: Data!, moment: inout Moment) {
        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Any] {
                moment.commentCount = json["data"] as! Int
            }
        } catch {
            print("Error parsing JSON: \(error)")
        }
    }
    
}
