//
//  TimeServices.swift
//  iOS Coding Challenge
//
//  Created by Phạm Phi Phúc on 6/1/17.
//  Copyright © 2017 Pham Phi Phuc. All rights reserved.
//

import Foundation

class TimeServices {
    
    static func dateFromString(_ dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "YYYY-MM-DD'T'HH:mm:ssZZZZZ"
        
        return dateFormatter.date(from: dateString)!
    }
    
    static func timeIntervalFromNow(_ date: Date) -> String {
        let elapsed = Date().timeIntervalSince(date)
        let dateTimeFormatter = DateComponentsFormatter()
        dateTimeFormatter.unitsStyle = .full
        dateTimeFormatter.allowedUnits = [.year, .month, .day, .hour, .minute, .second]
        dateTimeFormatter.maximumUnitCount = 1
        return dateTimeFormatter.string(from: elapsed)!
    }
    
}
