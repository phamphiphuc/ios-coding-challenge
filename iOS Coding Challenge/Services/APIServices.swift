//
//  APIServices.swift
//  iOS Coding Challenge
//
//  Created by Phạm Phi Phúc on 5/31/17.
//  Copyright © 2017 Pham Phi Phuc. All rights reserved.
//

import Foundation
import UIKit

typealias Handler = (Data?, URLResponse?, Error?) -> Void

class APIServices {
    
    fileprivate let baseURL = "https://thedemoapp.herokuapp.com"
    fileprivate let post = "/post"
    fileprivate let likeCount = "/likeCount"
    fileprivate let commentCount = "/commentCount"
    
    static let shared = APIServices()    
    
    func getMoments() {
        let taskGroup = DispatchGroup()
        
        let jsonServices = JSONServices.shared
        var moments: [Moment] = []
        let momentsURL = "\(baseURL)\(post)"
        
        taskGroup.enter()
        getDataFromURL(momentsURL) { (data, response, error) in
            guard error == nil, data != nil else { return }
            
            guard let result = jsonServices.parseMoments(data: data!), result != nil else { return }
            
            for moment in result {
                moments.append(moment)
            }
            taskGroup.leave()
        }
        
        for var moment in moments {
            taskGroup.enter()
            self.getLikeCount(momentId: moment._id) { (data, response, error) in
                guard error == nil, data != nil else { return }
                jsonServices.parseLikeCount(data: data!, moment: &moment)
                taskGroup.leave()
            }
            
            taskGroup.enter()
            self.getCommentCount(momentId: moment._id) { (data, response, error) in
                guard error == nil, data != nil else { return }
                jsonServices.parseCommentCount(data: data!, moment: &moment)
                taskGroup.leave()
            }
        }
        
        taskGroup.notify(queue: DispatchQueue.main, work: DispatchWorkItem(block: {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.moments = moments
        }))
    }
    
    func getAllPosts(handler: @escaping Handler) {
        let url = "\(baseURL)\(post)"
        getDataFromURL(url, handler: handler)
    }
    
    func getLikeCount(momentId id: Int, handler: @escaping Handler) {
        let url = "\(baseURL)\(post)/\(id)\(likeCount)"
        getDataFromURL(url, handler: handler)
    }
    
    func getCommentCount(momentId id: Int, handler: @escaping Handler) {
        let url = "\(baseURL)\(post)/\(id)\(commentCount)"
        getDataFromURL(url, handler: handler)
    }
    
    fileprivate func getDataFromURL(_ URLString: String, handler: @escaping Handler) {
        let request = URLRequest(url: URL(string: URLString)!, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 30)
        URLSession.shared.dataTask(with: request, completionHandler: handler).resume()
    }
}
