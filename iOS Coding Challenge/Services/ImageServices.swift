//
//  ImageServices.swift
//  iOS Coding Challenge
//
//  Created by Phạm Phi Phúc on 6/2/17.
//  Copyright © 2017 Pham Phi Phuc. All rights reserved.
//

import Foundation
import UIKit

class ImageServices {
    
    static let shared = ImageServices()
    
    private var cachedImages: [String: UIImage]
    
    private init() {
        cachedImages = [String: UIImage]()
    }
    
    func getImage(urlString: String, for imageView: UIImageView) {
        if (getCachedImage(imageKey: urlString) != nil) {
            imageView.image = getCachedImage(imageKey: urlString)
            return           
        }
        
        let request = URLRequest(url: URL(string: urlString)!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 30)
        URLSession.shared.dataTask(with: request, completionHandler: { [unowned self] (data, response, error) in
            guard error == nil, data != nil else { return }
            guard let image = UIImage(data: data!), image != nil else { return }
            imageView.image = image
            self.cacheImage(image, imageKey: urlString)
        }).resume()
    }
    
    fileprivate func cacheImage(_ image: UIImage, imageKey key: String) {
        cachedImages[key] = image
        let cacheData = UIImagePNGRepresentation(image)!
        let imagePath = imagePathForKey(key)
        do {
            try cacheData.write(to: imagePath, options: .atomic)
        } catch {
            print(error)
        }
    }
    
    fileprivate func getCachedImage(imageKey key: String) -> UIImage? {
        var image = cachedImages[key]
        
        if image == nil {
            let imagePath = imagePathForKey(key)
            if FileManager.default.fileExists(atPath: imagePath.path) {
                image = UIImage(contentsOfFile: imagePath.path)
                cachedImages[key] = image
            } else {
                return nil
            }
        }
        
        return image
    }
    
    fileprivate func imagePathForKey(_ key: String) -> URL {
        let documentDirectory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        return (documentDirectory?.appendingPathComponent(key.components(separatedBy: "/").last!))!
    }
    
}
