//
//  MomentsViewController.swift
//  iOS Coding Challenge
//
//  Created by Phạm Phi Phúc on 5/30/17.
//  Copyright © 2017 Pham Phi Phuc. All rights reserved.
//

import UIKit

class MomentsViewController: UITableViewController, MomentsDelegate {
    
    weak var indicatorView: UIActivityIndicatorView!
    
    let cellIdentifier = "Moment"
    
    var moments: [Moment] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Moments"
        
        setUpRowHeight()
        setUpIndicatorView()
    }
    
    func setUpRowHeight() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = tableView.frame.height
    }
    
    func setUpIndicatorView() {
        let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        self.indicatorView = indicatorView
        tableView.backgroundView = indicatorView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if moments.count == 0 {
            indicatorView.startAnimating()
        }
    }
    
    func didFinishGetMoments(_ moments: [Moment]) {
        self.moments = moments
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moments.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Can't figure out why this function didn't get called after reloading data
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MomentTableViewCell
        
        let moment = self.moments[indexPath.row]
        
        cell.userName.text = moment.user.fullName
        cell.message.text = moment.message
        cell.postTime.text = "\(TimeServices.timeIntervalFromNow(moment.postAt)) ago"
        cell.likeCount.text = "\(moment.likeCount!) \(moment.likeCount == 1 ? "like" : "likes")"
        cell.commentCount.text = "\(moment.commentCount!) \(moment.commentCount == 1 ? "comment" : "comments")"
        cell.avatar.layer.cornerRadius = cell.avatar.frame.size.width / 2
        cell.avatar.clipsToBounds = true
        ImageServices.shared.getImage(urlString: moment.imageURL, for: cell.picture)
        ImageServices.shared.getImage(urlString: moment.user.imageURL, for: cell.avatar)
        
        return cell
    }

}
