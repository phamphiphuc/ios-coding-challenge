//
//  MomentTableViewCell.swift
//  iOS Coding Challenge
//
//  Created by Phạm Phi Phúc on 5/30/17.
//  Copyright © 2017 Pham Phi Phuc. All rights reserved.
//

import UIKit

class MomentTableViewCell: UITableViewCell {

    @IBOutlet weak var wrapper: UIView!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var postTime: UILabel!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var iconLike: UIImageView!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var iconComment: UIImageView!
    @IBOutlet weak var commentCount: UILabel!
    
}
