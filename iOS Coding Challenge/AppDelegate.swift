//
//  AppDelegate.swift
//  iOS Coding Challenge
//
//  Created by Phạm Phi Phúc on 5/30/17.
//  Copyright © 2017 Pham Phi Phuc. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var delegate: MomentsDelegate = MomentsViewController()
    
    var moments: [Moment] = [] {
        didSet {
            delegate.didFinishGetMoments(moments)
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        APIServices.shared.getMoments()
        return true
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
        APIServices.shared.getMoments()
    }

}

protocol MomentsDelegate {
    func didFinishGetMoments(_ moments: [Moment])
}

